QT = core-private
CONFIG += qt warn_on console
CONFIG += release
CONFIG -= debug debug_and_release
macx:CONFIG -= app_bundle

DEFINES += QT_MOC QT_NO_CAST_FROM_ASCII QT_NO_CAST_FROM_BYTEARRAY QT_NO_COMPRESS QT_NO_QCONFIG

include(moc.pri)

SOURCES += main.cpp
